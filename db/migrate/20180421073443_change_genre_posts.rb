class ChangeGenrePosts < ActiveRecord::Migration[5.2]
  def change
    def up 
     add_column :posts, :genre, :string
   end 
   def down 
    remove_column :posts, :genre, :boolean
   end 
  end
end
