class Post < ApplicationRecord
  mount_base64_uploader :image, ImageUploader
  validates_presence_of :title, :content
end
