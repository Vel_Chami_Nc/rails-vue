class PostsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_post, only: [:show,:destroy, :edit, :update]

  def index 
    @posts = Post.all 
    respond_to do |format|
      format.html
      format.json { render json: @posts, status: :ok }
    end 
  end 
  
  def create
    @post = Post.create(post_params)
    if @post.save!
      render json: @post, status: :ok
    end
  end

  def edit
  end 
 
  def update
    if @post.update!(post_params)
      render json: @post, status: :ok
    end 
  end 

  def show 
    render json: @post, status: :ok
  end 

  def destroy
    @post.destroy
  end 

  private 

  def set_post
    @post = Post.find(params[:id])
  end 

  def post_params 
    params.require(:post).permit(:title, :content,:genre, :image)
  end 

end
