class PostSerializer < ActiveModel::Serializer
  attributes :id, :title, :content, :genre, :image
end