;(function () {
  "use strict"

  $(document).ready(function () {

   Vue.use(VeeValidate);
   Vue.use(VueResource);

   Vue.component('post-edit',{
    template: "#post-edit",
    props:{
      post: Object,
      index: Number
    },
    data: function(){
      return{
        edit: false
      }
    },
    methods:{
      onFileChanged(event) {
        let files = event.target.files || e.dataTransfer.files;
         if (!files.length)
              return;
         this.createImage(files[0]);
      },
      createImage(file) {
          let reader = new FileReader();
          let vm = this;
          reader.onload = (e) => {
              vm.post.image = e.target.result;
          };
          reader.readAsDataURL(file);
      },
      newPost: function(){
          let that =this;
          let data = that.post
          console.log(data);
          console.log(that.post.image);
          that.$validator.validateAll().then(function(){
            axios.post('http://localhost:5000/posts.json', data)
             .then(function(response) {
               console.log(response.data)
               that.posts.push(response.data)
               that.post = {}
             })
          })
        },
      deletePost:function(){
        let url = "http://localhost:5000/posts/" +  this.post.id + ".json";
        axios.delete(url)
        .then(response => this.$parent.posts.splice(this.index, 1));
      },
      editPost: function(){
        let that = this
        let url = "http://localhost:5000/posts/" +  this.post.id + ".json";
        let data = that.post
        console.log(url)
        console.log(this.edit)
          this.$validator.validateAll().then(function(){
           axios.put(url,data)
             .then(function(response){
                 data = response.data
                 that.edit = false;
                 Vue.set(that.post, 'image', that.post.image);
           })
         })
      }
    }
   });

   var vm = new Vue({
      el: "#app",
      data: function () {
        return {
          message: "Hello rails with vue",
          post:{
            id:null,
            title:"",
            content:"",
            image: '',
            genre:''
          },
          api: 'http://localhost:5000/posts.json',
          posts:[],
        }
      },
      mounted() {
        //do something after mounting vue instance
        this.getPosts();
      },
      methods: {
        // removePost:function(post, event){
        //   console.log(post.id)
        //   var index = event.target.getAttribute('data-index')
        //   let url = "http://localhost:5000/posts/" +  post.id + ".json";
        //   axios.delete(url)
        //     .then(response => this.posts.splice(index, 1));
        // },
        onFileChanged(event) {
          let files = event.target.files || e.dataTransfer.files;
           if (!files.length)
                return;
            this.createImage(files[0]);
        },
        createImage(file) {
            let reader = new FileReader();
            let vm = this;
            reader.onload = (e) => {
                vm.post.image = e.target.result;
            };
            reader.readAsDataURL(file);
        },
        removeImage: function (e) {
          this.post.image = "";
        },
        getPosts: function(){
          this.$http.get('http://localhost:5000/posts.json')
            .then(function(response){
              this.posts = response.data;
          });
        },
        newPost: function(){
          let that =this;
          let data = that.post
          console.log(data);
          console.log(that.post.image);
          that.$validator.validateAll().then(function(){
            axios.post('http://localhost:5000/posts.json', data)
             .then(function(response) {
               console.log(response.data)
               that.posts.push(response.data)
               that.post = {}
             })
          })
        }
      }
  })

 });


})();
